# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect, HttpResponse


def splash(request):
    return render(request, 'login/splash.html')


def sign_up(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('login')
    else:
        form = UserCreationForm()
    return render(request, 'login/signup.html', {'form': form})


def login_page(request):
    if request.method == 'POST':
        login(request, user)
        return redirect('/')  # TODO rdr to the profile here
    elif request.method == 'GET':
        return HttpResponse("<h1> awe brah </h1>")
    return render(request, 'login/login.html')
